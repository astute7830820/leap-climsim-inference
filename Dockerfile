FROM python:3.10
#
WORKDIR /code

#
RUN pip install --upgrade pip
RUN pip install poetry

#
COPY ./pyproject.toml /code/
COPY ./poetry.lock /code/

COPY ./main.py /code/app/main.py
COPY ./.env /code/app/.env

RUN poetry install --no-interaction --no-ansi

CMD ["poetry", "run", "uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "80"]
