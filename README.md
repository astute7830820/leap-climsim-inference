# Inference service

This project contains code for inference service of the climsim model. The service is implemented using FastAPI and is deployed using Docker.